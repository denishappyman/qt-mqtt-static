FROM ubuntu:18.04

LABEL maintainer="Denis Gerasimov <denishappyman@gmail.com>"

RUN apt -y update && apt -y upgrade
RUN apt -y install --no-install-recommends apt-utils build-essential cmake gdb git iputils-ping nano perl python valgrind wget && apt -y install mesa-common-dev libegl1-mesa-dev libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev devscripts ubuntu-dev-tools debhelper dh-make devscripts && apt -y install zlib1g-dev libssl-dev libnss3-dev libpq-dev libsqlite3-dev libxslt-dev libxml2-dev libjpeg-dev libpng-dev libopus-dev && apt -y install libxcursor-dev libxcb1-dev libxcb-xkb-dev libx11-xcb-dev libxrender-dev libfontconfig1 libfontconfig1-dev libfontconfig libfreetype6 libxi-dev libxcb-xinerama0-dev && apt -y install libxtst-dev libxss-dev libxext6 libxss1-dbg libxcomposite-dev wine-stable && export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig && apt -y autoremove && apt -y autoclean && apt -y update && rm -rf /var/lib/apt/lists/*

RUN cd /opt && wget -q http://download.qt.io/official_releases/qt/5.12/5.12.0/single/qt-everywhere-src-5.12.0.tar.xz && tar xf qt-everywhere-src-5.12.0.tar.xz && rm qt-everywhere-src-5.12.0.tar.xz && cd qt-everywhere-src-5.12.0 && ./configure -opensource -confirm-license -release -static -qt-xcb -qt-libpng -fontconfig -feature-freetype -system-freetype FREETYPE_INCDIR=/usr/include/freetype2 -qpa xcb -qt-libjpeg -nomake tests -nomake examples -no-compile-examples && make -j $(($(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1)+2)) -s && make install && cd /opt && rm -rf qt-everywhere-src-5.12.0;  exit 0

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/Qt-5.12.0/bin

RUN cd $(qmake -query QT_INSTALL_LIBS) \
&& git clone git://code.qt.io/qt/qtmqtt.git --branch v5.14.2 --single-branch \
&&  cd qtmqtt && qmake && make \
&&  make install &&  cd .. && rm -r qtmqtt

WORKDIR /app

# start app
CMD ["/bin/bash"]
